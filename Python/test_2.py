## \file tes_2.py
#  \brief Documentation for this module.
#

## \brief Documentation for file
# \file file used to test Doxygen on python
#

## \brief Documentation for a function.
# Does some stuff
# @param [in] test for anything
#  More details.
def func(int test):
    test=0
    n=5
    pass

## Documentation for a class.
#
#  More details.
class PyClass:

    ## The constructor.
    def __init__(self):
        self._memVar = 0

    ## Documentation for a method.
    #  @param self The object pointer.
    def PyMethod(self):
        pass

    ## A class variable.
    classVar = 0

    ## @var _memVar
    #  a member variable