//! \file Bayoudha.c
//! \brief Description: This file conducts stress testing for the pe2pe dma communication. The goal is to assess the performance under load by executing read and write operations between each Processing Element (PE) SRAM.
//!
//! The test involves a specified number of iterations and buffer sizes for both writing to and reading from the memory.
//! \author      Mohamed Amine Bayoudha
//! \date        18.04.2024
//!

/*======================================================================================================================================================================================================================================================
                                                                                                    Section: Includes Declarations
=======================================================================================================================================================================================================================================================*/
#include <s2app.h>
#include <hl_s2.h>
#include <rf_top.h>
/*======================================================================================================================================================================================================================================================
==============================================================================================================
       ==========================================================================================================================================
=======================================================================================================================================================================================================================================================*/


//! \name Memory addresses
//! \brief List of addresses used for this application
//! \{

//! \def QPE_OFFSET
//! \brief Offset to QPE address.
//! \def CMD_DMA_RD
//! \brief Address to DMA read command register. 
//! \def CMD_DMA_WR
//! \brief Address to DMA write command register.  
//! \def CMD_DMA_SDRAM_RD
//! \brief Address to SDRAM read command register. 
//! \def CMD_DMA_SDRAM_WR
//! \brief Address to SDRAM write command register.
//! \}

//! \def STATUS_DMA_DONE_MASK
//! \brief DMA done mask. 
//! \def CLEAR_DMA_CMD
//! \brief Clear DMA register command. 
//! \def BUFSIZE
//! \brief Buffer size. 
//! \def FAIL_RET
//! \brief ... 
    
/*======================================================================================================================================================================================================================================================
                                                                                                    Section: Defines Declarations
=======================================================================================================================================================================================================================================================*/
#define QPE_OFFSET                  0xF0000000U
#define CMD_DMA_RD                  0x1U
#define CMD_DMA_WR                  0x2U
#define CMD_DMA_SDRAM_RD            0x4U
#define CMD_DMA_SDRAM_WR            0x8U
#define STATUS_DMA_DONE_MASK        0x2000U
#define CLEAR_DMA_CMD               0x1U
/* Cfg */
#define BUFSIZE                     2560U
#define FAIL_RET                    0xbadc0dedu
#define SUCC_RET                    0xcafebabe //!< Testing description after the member
/*======================================================================================================================================================================================================================================================
========================================================================================================================================================================================================================================================
=======================================================================================================================================================================================================================================================*/
 
 
 
 

/*======================================================================================================================================================================================================================================================
                                                                                                    Section: Static Variable Declarations
=======================================================================================================================================================================================================================================================*/
//! \name Static variables
//! \{

//! \brief The local identifier of the PE

static uint32_t pe_id;
//! \}

/*======================================================================================================================================================================================================================================================
========================================================================================================================================================================================================================================================
=======================================================================================================================================================================================================================================================*/
 
 
 
 
 
 
/*======================================================================================================================================================================================================================================================
                                                                                                    Section: Variable Declarations
=======================================================================================================================================================================================================================================================*/
//! \cond
//! \endcond
//! \name Variables
//! \{
//!
volatile char testing_if_automaticly_updates; //!< Testing if the updates are pushed automatically in the web page
volatile uint32_t ptr_wr[BUFSIZE] __attribute__((aligned(4))); //!< word address
volatile uint32_t ptr_rd[BUFSIZE] __attribute__((aligned(4))); //!< Second word address

//! \}

/*======================================================================================================================================================================================================================================================
========================================================================================================================================================================================================================================================
=======================================================================================================================================================================================================================================================*/
 
/*======================================================================================================================================================================================================================================================
                                                                                                    Section: Private Function Declarations
=======================================================================================================================================================================================================================================================*/
 
 

//! \brief This function is responsible for the PE2PE communication using DMA.
//! \param [in] src_sram_PE     PE src SRAM address
//! \param [in] dest_qpe_x      Destination QPE_X
//! \param [in] dest_qpe_y      Destination QPE_Y
//! \param [in] dest_pe_id      Destination PE ID
//! \param [in] dest_sram_PE    PE destination SRAM address
//! \param [in] transfer_words  Number of words to transfer
//! \param [in] wr_en           TRUE : means write / FALSE : means read
//! \author                     Mohamed Amine Bayoudha
//! \date                       18.04.2024
//! 

void pe2pe_dma(uint32_t src_sram_PE, uint8_t dest_qpe_x, uint8_t dest_qpe_y, uint8_t dest_pe_id, uint32_t dest_sram_PE, uint16_t transfer_words, bool wr_en)
{
    COMMS->LOCAL_ADDR = src_sram_PE;
    COMMS->REMOTE_ADDR = (uint32_t)(QPE_OFFSET + (uint32_t)((dest_qpe_x << 22u) | (dest_qpe_y << 19u) | (dest_pe_id << 17u)) + (uint32_t)dest_sram_PE);
    COMMS->DMA_LEN = transfer_words;
    COMMS->DMA_CMD = (wr_en ? CMD_DMA_WR : CMD_DMA_RD);
}
 

//! \brief This function is responsible for the PE2PE writing using DMA.
//! \param [in] src_sram_PE     PE src SRAM address
//! \param [in] dest_qpe_x      Destination QPE_X
//! \param [in] dest_qpe_y      Destination QPE_Y
//! \param [in] dest_pe_id      Destination PE ID
//! \param [in] dest_sram_PE    PE destination SRAM address
//! \param [in] transfer_words  Number of words to transfer
//! \author                     Mohamed Amine Bayoudha
//! \date                       18.04.2024

void pe2pe_dma_wr(uint32_t src_sram_PE, uint8_t dest_qpe_x, uint8_t dest_qpe_y, uint8_t dest_pe_id, uint32_t dest_sram_PE, uint16_t transfer_words)
{
    pe2pe_dma(src_sram_PE, dest_qpe_x, dest_qpe_y, dest_pe_id, dest_sram_PE, transfer_words, true);
}
 

//! \brief This function is responsible for the PE2PE reading using DMA.
//! \param [in] src_sram_PE     PE src SRAM address
//! \param [in] dest_qpe_x      Destination QPE_X
//! \param [in] dest_qpe_y      Destination QPE_Y
//! \param [in] dest_pe_id      Destination PE ID
//! \param [in] dest_sram_PE    PE destination SRAM address
//! \param [in] transfer_words  Number of words to transfer
//! \author                     Mohamed Amine Bayoudha
//! \date                       18.04.2024


void pe2pe_dma_rd(uint32_t src_sram_PE, uint8_t dest_qpe_x, uint8_t dest_qpe_y, uint8_t dest_pe_id, uint32_t dest_sram_PE, uint16_t transfer_words)
{
    pe2pe_dma(src_sram_PE, dest_qpe_x, dest_qpe_y, dest_pe_id, dest_sram_PE, transfer_words, false);
}
 

//! \brief This function is responsible for waiting until DMA transfer is done.
//! \return ret_val : boolean value confirming that the dma transfer is done
//! \author                     Mohamed Amine Bayoudha
//! \date                       18.04.2024


bool dma_done(void)
{
    bool ret_val = false;
         volatile uint32_t status = COMMS->DMA_STATUS;
 
    status = COMMS->DMA_STATUS;
 
    while ((status & STATUS_DMA_DONE_MASK) == 0x0)
    {
        status = COMMS->DMA_STATUS;
    }
 
    COMMS->DMA_CLEAR = CLEAR_DMA_CMD;
    ret_val = true;
    return ret_val;
}
 
/*======================================================================================================================================================================================================================================================
========================================================================================================================================================================================================================================================
=======================================================================================================================================================================================================================================================*/
 
/*======================================================================================================================================================================================================================================================
                                                                                                    Main Function
=======================================================================================================================================================================================================================================================*/
int main()
{
    uint32_t ret_val = SUCC_RET;
    pe_id = getMyPEID();
    if (pe_id == 0x26)
    {
        for (uint16_t i = 0; i < BUFSIZE; i++)
        {
            ptr_wr[i] = (2 * i + i);
            ptr_rd[i] = 0;
        }
 
        pe2pe_dma_wr((uint32_t) & (ptr_wr[0]), 1, 2, 3, 0x8600, BUFSIZE);
 
        while (dma_done() == false)
            ;
 
        pe2pe_dma_rd((uint32_t) & (ptr_rd[0]), 1, 2, 3, 0x8600, BUFSIZE);
 
        while (dma_done() == false)
            ;
 
        for (uint16_t i = 0; i < BUFSIZE; i++)
        {
            if (ptr_wr[i] != ptr_rd[i])
            {
                ret_val = FAIL_RET;
                break;
            }
        }
    }
 
    return ret_val;
}
/*======================================================================================================================================================================================================================================================
========================================================================================================================================================================================================================================================
=======================================================================================================================================================================================================================================================*/